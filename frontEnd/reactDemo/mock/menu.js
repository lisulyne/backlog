export default [
    {
      url: "/api/menu",
      method: "get",
      response: () => {
        return {
          code: 200,
          message: "ok",
          data: [
            {
                path: '/welcome',
                name: 'welcome',
                icon: 'smile',
                routes: [
                  {
                    path: '/welcome2',
                    name: 'one',
                    icon: 'smile',
                    exact: true,
                  },
                ],
              },
              {
                path: '/demo',
                name: 'demo',
                icon: 'heart',
                routes: [
                  {
                    path: '/demo/welcome',
                    name: 'two',
                    icon: 'heart',
                    routes: [
                      {
                        path: '/demo2/welcome2',
                        name: 'two2',
                        icon: 'heart',
                      },
                    ],
                  },
                ],
              },
              {
                path: '/about',
                name: 'about',
                icon: 'crown',
              },
          ]
        };
      }
    }
  ];