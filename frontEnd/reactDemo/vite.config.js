import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import vitePluginImp from 'vite-plugin-imp'
import { viteMockServe } from "vite-plugin-mock";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    react(),
    // mock模拟数据
    viteMockServe({
        mockPath: "./mock",//mock文件地址
        // localEnabled: false, // 开发打包开关
        // prodEnabled: false, // 生产打包开关
        // // 关闭mock的时候不让mock打包到最终代码内
        // injectCode: 
        //   `
        //     import { setupProdMockServer } from './mockProdServer';
        //     setupProdMockServer();
        //   `
        // ,
        // logger: true, //是否在控制台显示请求日志
        supportTs: false //打开后，可以读取 ts 文件模块。 请注意，打开后将无法监视.js 文件
    }),
    // css按需加载
    vitePluginImp({
      libList: [
        {
          libName: 'antd',
          style: (name) => `antd/es/${name}/style`,
        },
      ],
    })
  ],
  css: {
    preprocessorOptions: {
      less: {
        // modifyVars: { 'primary-color': '#13c2c2' },
        javascriptEnabled: true,
      },
    },
  },
  resolve: {
    alias: [
      // fix less import by: @import ~
      // less import no support webpack alias '~' · Issue #2185 · vitejs/vite
      { find: /^~/, replacement: '' },
    ],
  },
})
