import React, { Component } from 'react'
import { Layout, Menu ,Avatar,Row, Col ,Dropdown  } from 'antd';
const { Header, Sider, Content } = Layout;
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  UserOutlined,
  VideoCameraOutlined,
  UploadOutlined,
  BellFilled,
  DownOutlined ,
} from '@ant-design/icons';
import "antd/dist/antd.css"
import './App.css'

const menu = (
  <Menu>
    <Menu.Item key="0">
      <a href="https://www.antgroup.com">个人中心</a>
    </Menu.Item>
    {/* <Menu.Item key="1">
      <a href="https://www.aliyun.com"></a>
    </Menu.Item>
    <Menu.Divider /> */}
    <Menu.Item key="3">退出</Menu.Item>
  </Menu>
);

export default class App extends Component {
  state = {
    collapsed: false,
  };
  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };
  render() {
    return (
      <Layout>
        <Header style={{ position: 'fixed', zIndex: 1, width: '100%' ,backgroundColor:'#ebf0f2',padding:0}}>
          <Row>
            <Col span={12} style={{paddingLeft:24}}>L O G O</Col>
            <Col span={12} style={{textAlign:'right',paddingRight:50}}> 
              <ul className='nav' style={{justifyContent:'end'}}>
                <li className="nav-item" style={{margin:'0 20px'}}>
                  <BellFilled style={{ fontSize: '24px'}} />
                  </li>
                <li className="nav-item">
                  <Dropdown overlay={menu}  trigger={['click']} placement="bottomCenter" arrow>
                    <span style={{cursor:'pointer'}}>
                      <Avatar size={32} icon={<UserOutlined />} />
                    </span>
                  </Dropdown> 
                </li>
              </ul>    
            </Col>
          </Row>
        </Header>
        <Layout style={{ marginTop: 64 }}>
          <Sider trigger={null} collapsible collapsed={this.state.collapsed}>
            <div style={{
              height: 40,
              textAlign:'center'
            }}>
              {React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                className: 'trigger',
                onClick: this.toggle,
              })}
            </div>
            <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
              <Menu.Item key="1" icon={<UserOutlined />}>
                nav 1
              </Menu.Item>
              <Menu.Item key="2" icon={<VideoCameraOutlined />}>
                nav 2
              </Menu.Item>
              <Menu.Item key="3" icon={<UploadOutlined />}>
                nav 3
              </Menu.Item>
            </Menu>
          </Sider>
          <Content>
            
          </Content>
        </Layout>
      </Layout>
    )
  }
}
