import axios from 'axios';

const API_MENU = '/api/menu';

async function loadmenus() {
    let menus=[];
    await axios.get(API_MENU).then(res => {
        console.log(res.data);
        menus = res.data.data;
    });
    console.log(menus);
    return menus;
};

export default loadmenus;