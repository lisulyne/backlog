import React, { useState, useEffect }from 'react';
import { Button, Result, Avatar, Menu, Input ,Dropdown,Tooltip,Table } from 'antd';
import { HeartOutlined, CrownOutlined, SmileOutlined,EllipsisOutlined,UserOutlined,DownOutlined, QuestionCircleOutlined} from '@ant-design/icons';
import ProLayout, { PageContainer } from '@ant-design/pro-layout';
import ProTable, { TableDropdown } from '@ant-design/pro-table';
import loadmenus from './api/menu'
import "antd/dist/antd.css"

const IconMap = {
  smile: <SmileOutlined/>,
  heart: <HeartOutlined/>,
  crown: <CrownOutlined/>,
};
// routes:dataHandle(item.routes)
const dataHandle = async () => {
  let menus = await loadmenus();
  console.log("menus")
  console.log(menus)
  return menuhandle(menus);
  // return menus.map(item => {
  //   const localItem = { ...item,icon:IconMap[item.icon]};
  //   return localItem;
  // }); 
};

const menuhandle = (menus) => menus.map(item => {
  console.log("item")
  const localItem = {...item,icon:IconMap[item.icon],routes:item.routes && menuhandle(item.routes)};
  console.log(localItem);
  return localItem;
})

// react组件
const AvaterDropdownMenu = () => (
  <Dropdown key="menu" overlay={avaterMenu} >
      <Avatar shape="circle" size="32" src="https://joeschmoe.io/api/v1/random" style={{cursor:'pointer'}} />
  </Dropdown>
);
// react元素结点
const avaterMenu = (
  <Menu>
    <Menu.Item key="0">
      <a href="https://www.antgroup.com">个人中心</a>
    </Menu.Item>
    <Menu.Divider />
    <Menu.Item key="3">退出</Menu.Item>
  </Menu>
)
const toolMenu = (
  <Menu>
    <Menu.Item key='1s'>
      <a target="_blank" rel="noopener noreferrer" href="http://www.alipay.com/">
        1st menu item
      </a>
    </Menu.Item>
    <Menu.Item key='2s'>
      <a target="_blank" rel="noopener noreferrer" href="http://www.taobao.com/">
        2nd menu item
      </a>
    </Menu.Item>
  </Menu>
);

export default () => {
  const [data,setData] = useState([]);
  const [loading,setLoading] = useState(false);
  const [pagination,setPagination] = useState({
    current: 1,
    pageSize: 10,
  });


  useEffect(() => {
    console.log('---组件加载或者数据更新完了---');
    fetchData();
    return ()=>{
      console.log('---组件将要卸载了---');
    }
  })

  // const columns = [
  //   {
  //     title: '应用名称',
  //     width: 80,
  //     dataIndex: 'name',
  //     render: (_) => <a>{_}</a>,
  //   },
  //   {
  //     title: '容器数量',
  //     dataIndex: 'containers',
  //     align: 'right',
  //     sorter: (a, b) => a.containers - b.containers,
  //   },
  //   {
  //     title: '状态',
  //     width: 80,
  //     dataIndex: 'status',
  //     initialValue: 'all',
  //     valueEnum: {
  //       all: { text: '全部', status: 'Default' },
  //       close: { text: '关闭', status: 'Default' },
  //       running: { text: '运行中', status: 'Processing' },
  //       online: { text: '已上线', status: 'Success' },
  //       error: { text: '异常', status: 'Error' },
  //     },
  //   },
  //   {
  //     title: '创建者',
  //     width: 80,
  //     dataIndex: 'creator',
  //     valueEnum: {
  //       all: { text: '全部' },
  //       付小小: { text: '付小小' },
  //       曲丽丽: { text: '曲丽丽' },
  //       林东东: { text: '林东东' },
  //       陈帅帅: { text: '陈帅帅' },
  //       兼某某: { text: '兼某某' },
  //     },
  //   },
  //   {
  //     title: (
  //       <>
  //         创建时间
  //         <Tooltip placement="top" title="这是一段描述">
  //           <QuestionCircleOutlined style={{ marginLeft: 4 }} />
  //         </Tooltip>
  //       </>
  //     ),
  //     width: 140,
  //     key: 'since',
  //     dataIndex: 'createdAt',
  //     valueType: 'date',
  //     sorter: (a, b) => a.createdAt - b.createdAt,
  //   },
  //   {
  //     title: '备注',
  //     dataIndex: 'memo',
  //     ellipsis: true,
  //     copyable: true,
  //   },
  //   {
  //     title: '操作',
  //     width: 180,
  //     key: 'option',
  //     valueType: 'option',
  //     render: () => [
  //       <a key="link">链路</a>,
  //       <a key="link2">报警</a>,
  //       <a key="link3">监控</a>,
  //       <TableDropdown
  //         key="actionGroup"
  //         menus={[
  //           { key: 'copy', name: '复制' },
  //           { key: 'delete', name: '删除' },
  //         ]}
  //       />,
  //     ],
  //   },
  // ]
  // const valueEnum = {
  //   0: 'close',
  //   1: 'running',
  //   2: 'online',
  //   3: 'error',
  // };
  // const creators = ['付小小', '曲丽丽', '林东东', '陈帅帅', '兼某某'];
  // const tableListDataSource = [];
  // for (let i = 0; i < 5; i += 1) {
  //   tableListDataSource.push({
  //     key: i,
  //     name: 'AppName',
  //     containers: Math.floor(Math.random() * 20),
  //     creator: creators[Math.floor(Math.random() * creators.length)],
  //     status: valueEnum[Math.floor(Math.random() * 10) % 4],
  //     createdAt: Date.now() - Math.floor(Math.random() * 100000),
  //     memo: i % 2 === 1 ? '很长很长很长很长很长很长很长的文字要展示但是要留下尾巴' : '简短备注文案',
  //   });
  // }
  const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      sorter: true,
      render: name => `${name.first} ${name.last}`,
      width: '20%',
    },
    {
      title: 'Gender',
      dataIndex: 'gender',
      filters: [
        { text: 'Male', value: 'male' },
        { text: 'Female', value: 'female' },
      ],
      width: '20%',
    },
    {
      title: 'Email',
      dataIndex: 'email',
    },
  ];
  const fetchData = () => {
    setLoading(true);
    fetch(`https://randomuser.me/api?current=1&pageSize=10`)
      .then(res => res.json())
      .then(res => {
        console.log(res);
        setLoading(false);
        setData(res.results);
        setPagination({...params.pagination,total: 200})
        // this.setState({
        //   loading: false,
        //   data: data.results,
        //   pagination: {
        //     ...params.pagination,
        //     total: 200,
        //     // 200 is mock data, you should read it from server
        //     // total: data.totalCount,
        //   },
        // });
      });
  };
  return (
    <>
      <ProLayout
        navTheme="light"
        fixSiderbar
        menuHeaderRender={null} //不渲染 logo 和 title
        headerRender={false} //不渲染header,false情况下，rightContentRender设置无效
        location={{
          pathname: '/',
        }}
        menu={{request:async ()=> dataHandle()}}
        rightContentRender={() => (
          <div>
            <AvaterDropdownMenu/>
            <Dropdown key="more" overlay={toolMenu}>
              <Button
                style={{
                  border: 'none',
                  padding: '0 20px',
                }}
              >
                <EllipsisOutlined
                  style={{
                    fontSize: 20,
                    verticalAlign: 'top',
                  }}
                />
              </Button>
            </Dropdown>
          </div>
        )}
      >
      <PageContainer
          header={{
            style: {
              padding: '4px 16px',
              position: 'fixed',
              top: 0,
              width: '100%',
              left: 0,
              zIndex: 999,
              boxShadow: '0 2px 8px #f0f1f2',
            },
          }}
          extra={[
            <Input.Search
              key="search"
              style={{
                width: 240,
              }}
            />,
            <AvaterDropdownMenu/>,
            <Dropdown key="more" overlay={toolMenu}>
              <Button>
                <EllipsisOutlined
                  style={{
                    fontSize: 20,
                    verticalAlign: 'top',
                  }}
                />
              </Button>
            </Dropdown>,
          ]}
          style={{
            paddingTop: 48,
          }}
        >
          {/* <ProTable
            columns={columns}
            dataSource={tableListDataSource}
            request={(params, sorter, filter) => {
              // 表单搜索项会从 params 传入，传递给后端接口。
              console.log(params, sorter, filter);
              return Promise.resolve({
                data: tableListDataSource,
                success: true,
              });
            }}
            rowKey="key"
            pagination={{
              showQuickJumper: true,
            }}
            search={{
              optionRender: false,
              collapsed: false,
            }}
            dateFormatter="string"
            // headerTitle="表格标题"
            toolBarRender={() => [
              <Button key="show">查看日志</Button>,
            ]}
          /> */}
          <Table
            columns={columns}
            rowKey={record => record.login.uuid}
            dataSource={data}
            pagination={pagination}
            loading={loading}
            // onChange={this.handleTableChange}
          />
          </PageContainer>
      </ProLayout>
    </>
  );
};