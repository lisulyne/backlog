import CONSTPOOL from "./constant";
// 人性化输出时间
export const humanizeTime = (hour)=>{
    if(hour && hour>0){
      let oneDay=8,
      oneWeek=oneDay*5,
      oneMonth=oneDay*22,
      oneYear = oneMonth*12;
      if(hour<oneDay){
        return hour+"小时"
      }else if(oneDay<=hour && hour < oneWeek){
        return Math.floor(hour/oneDay)+"天"+Math.round(hour%oneDay)+"小时"
      }else if(oneWeek<=hour && hour < oneMonth){
        return Math.floor(hour/oneWeek)+"周"+Math.round(hour%oneWeek)+"小时"
      }else if(oneMonth<=hour && hour < oneYear){
        return Math.floor(hour/oneMonth)+"个月"+Math.round(hour%oneMonth)+"小时"
      }else{
        return "1年以上";
      }
    }else{
      return "";
    }
  }

//根据type和该type下值来获取该type对象信息 
  export const handleEventInfo = (type,value)=>{
    let eventInfo = {};
    switch(type){
      case CONSTPOOL.EVENT_INFO.status:
        eventInfo =  CONSTPOOL.EVENT_STATUS.find(item=>item.value==value);
        break;
      case CONSTPOOL.EVENT_INFO.category:
        eventInfo =  CONSTPOOL.EVENT_TYPE.find(item=>item.value==value);
        break;    
      case CONSTPOOL.EVENT_INFO.priority:
        eventInfo =  CONSTPOOL.EVENT_PRIORITY.find(item=>item.value==value);
        break;  
      default:
        eventInfo = CONSTPOOL.EVENT_INFO.empty        
    }
    return eventInfo;
  }

  