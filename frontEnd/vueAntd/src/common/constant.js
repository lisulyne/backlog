// 事件信息标记
const EVENT_INFO = {
  empty:{text: '', value: 0,color:''},
  status:1,
  category:2,
  priority:3
};

// 事件状态
const EVENT_STATUS = [
    { text: '未开始',label: '未开始', value: 0,color:''},
    { text: '准备中',label: '准备中', value: 1,color:'purple'},
    { text: '待处理',label: '待处理', value: 2,color:'blue'},
    { text: '处理中',label: '处理中', value: 3,color:'cyan'},
    { text: '待确认',label: '待确认', value: 4,color:'green'},
    { text: '确认中',label: '确认中', value: 5,color:'orange'},
    { text: '待发布',label: '待发布', value: 6,color:'pink'},
    { text: '已结束',label: '已结束', value: 7,color:'gray'},
  ]
  
// 事件类型
  const EVENT_TYPE = [
    { text: '其他', value: 0,color:'gray'},
    { text: '障害', value: 1,color:'red'},
    { text: '新规', value: 2,color:'blue'},
    { text: 'Q A', value: 3,color:'orange'},
  ]
// 事件优先度
  const EVENT_PRIORITY = [
    { text: '低', value: 0,color:'green'},
    { text: '中', value: 1,color:'orange'},
    { text: '高', value: 2,color:'pink'},
    { text: '急', value: 3,color:'red'},
  ]
  const CONSTPOOL = {};
  CONSTPOOL.EVENT_INFO = EVENT_INFO;
  CONSTPOOL.EVENT_STATUS = EVENT_STATUS;
  CONSTPOOL.EVENT_TYPE = EVENT_TYPE;
  CONSTPOOL.EVENT_PRIORITY = EVENT_PRIORITY;
export default CONSTPOOL;