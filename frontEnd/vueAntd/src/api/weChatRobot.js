/*
 * @Author: your name
 * @Date: 2022-02-22 19:37:18
 * @LastEditTime: 2022-02-22 20:01:20
 * @LastEditors: Please set LastEditors
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \vueAntd\src\api\weChatRobot.js
 */
import axios from "axios";

const WEBHOOK_URL = 'https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=28953105-21f5-4e39-823d-14b090a571de';

// 文本类型
// 参数	是否必填	说明
// msgtype	是	消息类型，此时固定为text
// content	是	文本内容，最长不超过2048个字节，必须是utf8编码
// mentioned_list	否	userid的列表，提醒群中的指定成员(@某个成员)，@all表示提醒所有人，如果开发者获取不到userid，可以使用mentioned_mobile_list
// mentioned_mobile_list	否	手机号列表，提醒手机号对应的群成员(@某个成员)，@all表示提醒所有人
// {
//     "msgtype": "text",
//     "text": {
//         "content": "广州今日天气：29度，大部分多云，降雨概率：60%",
//         "mentioned_list":["wangqing","@all"],
//         "mentioned_mobile_list":["13800001111","@all"]
//     }
// }

// markdown类型
// 参数	是否必填	说明
// msgtype	是	消息类型，此时固定为markdown
// content	是	markdown内容，最长不超过4096个字节，必须是utf8编码,
{/* 内置3种字体颜色
    <font color="info">绿色</font>
    <font color="comment">灰色</font>
    <font color="warning">橙红色</font> */}
// {
//     "msgtype": "markdown",
//     "markdown": {
//         "content": "实时新增用户反馈<font color=\"warning\">132例</font>，请相关同事注意。\n
//          >类型:<font color=\"comment\">用户反馈</font>
//          >普通用户反馈:<font color=\"comment\">117例</font>
//          >VIP用户反馈:<font color=\"comment\">15例</font>"
//     }
// }

// 图片类型
// 参数	是否必填	说明
// msgtype	是	消息类型，此时固定为image
// base64	是	图片内容的base64编码
// md5	是	图片内容（base64编码前）的md5值
// {
//     "msgtype": "image",
//     "image": {
//         "base64": "DATA",
//         "md5": "MD5"
//     }
// }

// 图文类型
// 参数	是否必填	说明
// msgtype	是	消息类型，此时固定为news
// articles	是	图文消息，一个图文消息支持1到8条图文
// title	是	标题，不超过128个字节，超过会自动截断
// description	否	描述，不超过512个字节，超过会自动截断
// url	是	点击后跳转的链接。
// picurl	否	图文消息的图片链接，支持JPG、PNG格式，较好的效果为大图 1068*455，小图150*150。
// {
//     "msgtype": "news",
//     "news": {
//        "articles" : [
//            {
//                "title" : "中秋节礼品领取",
//                "description" : "今年中秋节公司有豪礼相送",
//                "url" : "www.qq.com",
//                "picurl" : "http://res.mail.qq.com/node/ww/wwopenmng/images/independent/doc/test_pic_msg1.png"
//            }
//         ]
//     }
// }
const weChatMessage = (msg) => {
    axios.post(WEBHOOK_URL,msg)
}
