import axios from 'axios'
import CONSTPOOL from '../common/constant'

const API_USER_LIST = '/user/list';
const API_TASK_LIST = '/task/list';

export const getUserList = async (params) => {
    let dataList = []
    await axios.get(API_USER_LIST,{params:params})
    .then(res=>{
      console.info(res.data);
      dataList = res.data.data;
    }).catch(e=>{
        console.log('Error', e.message);
    });
    return dataList;
}

export const getTaskList = async (params) => {
    let dataList = []
    await axios.get(API_TASK_LIST,{params:params})
    .then(res=>{
      console.info(res.data);
      dataList = res.data.data;
    }).catch(e=>{
        console.log('Error', e.message);
    });
    return dataList;
}
export const TaskColumns = [
    {title: '项目名',dataIndex: 'projectName',with:50},
    {title: '状态',dataIndex: 'eventStatus',filters: CONSTPOOL.EVENT_STATUS,slots: { customRender: 'eventStatus' },with:150},    
    {title: '事件番号',dataIndex: 'eventId',sorter: true,with:200,slots: { customRender: 'eventId' }},
    {title: '事件标题',dataIndex: 'eventName',ellipsis:true,with:"30%"},
    {title: '事件类型',dataIndex: 'eventType',filters: CONSTPOOL.EVENT_TYPE,slots: { customRender: 'eventType' },with:150},
    {title: '优先度',dataIndex: 'eventPriority',filters: CONSTPOOL.EVENT_PRIORITY,slots: { customRender: 'eventPriority' },with:150},
    {title: '发生日',dataIndex: 'eventStarttime',sorter: true,with:150},
    {title: '登录者',dataIndex: 'createdUser',with:150},
    {title: '工数',dataIndex: 'worktime',slots: { customRender: 'worktime' },sorter: true,with:150},
    {title: '予定开始',dataIndex: 'planStarttime',sorter: true,with:150},
    {title: '予定结束',dataIndex: 'planEndtime',sorter: true,with:150},
    {title: '实际开始',dataIndex: 'starttime',sorter: true,with:150},
    {title: '实际结束',dataIndex: 'endtime',sorter: true,with:150},
    {title: '担当者',dataIndex: 'assignee',with:150},
    {title: '确认者',dataIndex: 'confirmor',with:150},
]

export const UserColumns = [
    {
      title: '姓名',
      dataIndex: 'name',
    },
    {
      title: '性别',
      dataIndex: 'gender',
      filters: [
        { text: '男', value: true },
        { text: '女', value: false},
      ],
      slots: { customRender: 'gender' },
    },
     {
      title: '生日',
      dataIndex: 'birthday',
      sorter: true,
    },
    {
      title: '邮编',
      dataIndex: 'code',
    },
      {
      title: '地址',
      dataIndex: 'address',
    },
  ];