import axios from 'axios'

const API_MENU_LIST = '/menu/list';

export const getMenuList = async (params) => {
    let dataList = []
    await axios.get(API_MENU_LIST,{params:params})
    .then(res=>{
      console.info(res.data);
      dataList = res.data.data;
    }).catch(e=>{
        console.log('Error', e.message);
    });
    return dataList;
}
