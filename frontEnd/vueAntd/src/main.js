import { createApp } from 'vue'

import antd from 'ant-design-vue'
import router from './router'
import store from './store'
import "ant-design-vue/dist/antd.min.css"

import App from './App.vue'

createApp(App)
.use(antd)
.use(router)
.use(store)
.mount('#app')