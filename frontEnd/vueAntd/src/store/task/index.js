// 任务仓库

// 属性
const state = {};

// 【修改数据】-必须同步函数
// 供actions使用
// 载荷形式分发：store.commit('mutationName', value | {key: value})
// 对象形式分发：store.commit({type: 'mutationName',key: value})
const mutations = {};

// 【触发mutations事件】-可异步
// 供外部使用:
// 载荷形式分发：store.dispatch('actionName',{key: value})
// 对象形式分发：store.dispatch({type: 'actionName',key: value})
const actions = {};

// 计算属性
const getters = {};

export default {
    state,
    mutations,
    actions,
    getters
}