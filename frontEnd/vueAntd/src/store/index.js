import { createStore } from 'vuex'

const store = createStore({
    // 存库模块化开发模式
    // modules:{

    // },
    // 全局对象
    state () {
        return {
          count: 0
        }
      },
    // 每个 mutation 都有一个字符串的事件类型 (type)和一个同步回调函数 (handler)
    mutations: {
        // 事件名，handler额外的参数，即 mutation 的载荷（payload）对象,通过store.commit('increment',1)。
        increment (state, payload) {
            state.count += payload
        }
    },
    // 可异步操作来提交mutation，Action 通过 store.dispatch('actionName') 方法触发
    actions: {
        asyncIncrement (context) {
            context.commit('increment')
          }
    }
  })

  export default store;