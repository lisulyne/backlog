import {createRouter,createWebHistory,createWebHashHistory} from 'vue-router'

// 1.引入页面组件(编译打包时，全部加载在一个文件里面)
// import SearchTable from '../components/SearchTable.vue';
// 1.引入页面组件(懒加载，编译打包时分别不同文件)
const Project = ()=>import('@/components/Project.vue')
const Report = ()=>import('@/components/Report.vue')
const WeekReport = ()=>import('@/components/WeekReport.vue')
const MonthReport = ()=>import('@/components/MonthReport.vue')
const SearchTable = ()=>import('@/components/SearchTable.vue')

const Home = ()=>import('@/pages/Home.vue')
const NotFound = ()=>import('@/pages/NotFound.vue')

// 2. 定义一些路由，每个路由都需要映射到一个组件
const routes = [
    { 
      name:'page',
      path: "/", 
      component:Home,
      redirect: 'task',
      children: [
        {name:'project', path: 'project', component: Project ,meta:{isAuth:true}},
        {name:'report', path: 'report/day', component: Report ,meta:{isAuth:true}},
        {name:'weekReport', path: 'report/week', component: WeekReport ,meta:{isAuth:true}},
        {name:'monthReport', path: 'report/month', component: MonthReport ,meta:{isAuth:true}},
        {name:'task', path: 'task', component: SearchTable },
      ]
    },
    {name:'404',path:'/:path(.*)',components:{default:NotFound,}}
  ]

// 3. 创建路由器，并导出  
const router =  createRouter({
  // 内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
  history: createWebHistory(),
  routes, // `routes: routes` 的缩写
})


// 路由前置拦截
router.beforeEach((to,from,next)=>{
    next()
})

export default router;