<!--
 * @Author: your name
 * @Date: 2022-02-21 10:14:02
 * @LastEditTime: 2022-02-22 12:54:16
 * @LastEditors: your name
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \vueAntd\README.md
-->
# 安装
- npm create vite@latest
- npm install
- npm run dev

# 插件

- npm i ant-design-vue@next --save

# 周期

- 组件激活：activated(){} 
- 组件失活：deactivated(){}

# 路由

路由缓存（保持数据不丢失，组件不会卸载）

```html
<keep-alive :include="['组件名1','组件名2']">
<router-view></router-view>
</keep-alive>
```

