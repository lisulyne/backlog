export default [
    {
      url: "/report/case",
      method: "get",
      response: () => {
        return {
          code: 200,
          message: "ok",
          data: [
            {value:"@natural(1, 100)", name:'障害'},
            {value:"@natural(1, 100)", name:'新规'},
            {value:"@natural(1, 100)", name:'通常'},
            {value:"@natural(1, 100)", name:'QA'}
          ]
        };
      }
    },
    {
        url: "/month/report",
        method: "get",
        response: () => {
          return {
            code: 200,
            message: "ok",
            data: [
                ['类型', '2012', '2013', '2014', '2015', '2016', '2017'],
                ['障害', '@natural(1, 100)',  '@natural(1, 100)',  '@natural(1, 100)',  '@natural(1, 100)',  '@natural(1, 100)',  '@natural(1, 100)'],
                ['新规', '@natural(1, 100)',  '@natural(1, 100)',  '@natural(1, 100)',  '@natural(1, 100)',  '@natural(1, 100)',  '@natural(1, 100)'],
                ['通常', '@natural(1, 100)',  '@natural(1, 100)',  '@natural(1, 100)',  '@natural(1, 100)',  '@natural(1, 100)',  '@natural(1, 100)'],
                ['QA', '@natural(1, 100)',  '@natural(1, 100)',  '@natural(1, 100)',  '@natural(1, 100)',  '@natural(1, 100)',  '@natural(1, 100)']
            ]
          };
        }
      },
      {
        url: "/week/trend",
        method: "get",
        response: () => {
          return {
            code: 200,
            message: "ok",
            data: [
                ['类型', '2022/02/04', '2022/02/11', '2022/02/18', '2022/02/25'],
                ['障害', '@natural(1, 10)',  '@natural(1, 10)',  '@natural(1, 10)',  '@natural(1, 10)'],
                ['新规', '@natural(1, 10)',  '@natural(1, 10)',  '@natural(1, 10)',  '@natural(1, 10)'],
                ['通常', '@natural(1, 10)',  '@natural(1, 10)',  '@natural(1, 10)',  '@natural(1, 10)'],
                ['QA', '@natural(1, 10)',  '@natural(1, 10)',  '@natural(1, 10)',  '@natural(1, 10)']
            ]
          };
        }
      },
      {
        url: "/month/trend",
        method: "get",
        response: () => {
          return {
            code: 200,
            message: "ok",
            data: [
                ['类型', '2022/01', '2022/02', '2022/03', '2022/04'],
                ['障害', '@natural(1, 30)',  '@natural(1, 30)',  '@natural(1, 30)',  '@natural(1, 30)'],
                ['新规', '@natural(1, 30)',  '@natural(1, 30)',  '@natural(1, 10)',  '@natural(1, 30)'],
                ['通常', '@natural(1, 30)',  '@natural(1, 30)',  '@natural(1, 30)',  '@natural(1, 30)'],
                ['QA', '@natural(1, 30)',  '@natural(1, 30)',  '@natural(1, 30)',  '@natural(1, 30)']
            ]
          };
        }
      }
  ];