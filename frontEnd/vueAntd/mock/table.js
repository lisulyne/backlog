export default [
    {
        url: "/user/list",
        method: "get",
        response: () => {
            return {
                "code": 200,
                "message": "ok",
                "data|0-30":[
                    {
                        'id|+1':1,
                        'name':'@cname',
                        'code': '@zip',
                        'birthday': '@date',
                        'gender':'@boolean',
                        'address': '@ctitle(10,30)'
                    }
                ]
            }
        }
    },
    {
        url: "/task/list",
        method: "get",
        response: () => {
            return {
                "code": 200,
                "message": "ok",
                "data|0-30":[
                    {
                        'id|+1':1,
                        'projectName|1':['SC','CL'],
                        'eventStatus': '@natural(0, 7)',
                        'eventId': /SC_NWT-\d{2,4}/,
                        'eventName':'@ctitle(10,30)',
                        'eventType': '@natural(0, 3)',
                        'eventPriority': '@natural(0, 3)',
                        'eventStarttime': '@date',
                        'createdUser':'@cname',
                        'worktime': '@float(0,8760,0,1)',
                        'planStarttime': '@date',
                        'planEndtime': '@date',
                        'starttime': '@date',
                        'endtime': '@date',
                        'assignee': '@cname',
                        'confirmor': '@cname',
                    }
                ]
            }
        }
    },
]