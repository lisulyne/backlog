export default [
    {
      url: "/menu/list",
      method: "get",
      response: () => {
        return {
          code: 200,
          message: "ok",
          data: [
            {
              path: '/project',
              name: '项目',
              icon: 'smile',
            },
              {
                path: '/task',
                name: '任务',
                icon: 'smile',
              },
              {
                path: '/report',
                name: '报告',
                icon: 'heart',
                subMenus: [
                   {
                    path: '/report/day',
                    name: '日报',
                    icon: 'heart',
                    },
                  {
                    path: '/report/week',
                    name: '周报',
                    icon: 'heart',
                  },
                  {
                    path: '/report/month',
                    name: '月报',
                    icon: 'heart',
                  },
                ],
              }
          ]
        };
      }
    }
  ];