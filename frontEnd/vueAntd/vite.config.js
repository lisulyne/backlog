import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import ViteComponents, { AntDesignVueResolver } from 'vite-plugin-components';
import { viteMockServe } from "vite-plugin-mock";
import { resolve } from 'path'
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    // and按需加载
    ViteComponents({
      customComponentResolvers: [AntDesignVueResolver()],
    }),
    // mock模拟数据
    viteMockServe({
      mockPath: "./mock",//mock文件地址
      supportTs: false
    })
  ],
  css: {
    loaderOptions: {
      less: {
        javascriptEnabled: true
      }
    }
  },
  // 配置是路径别名
  resolve: {
    alias: {
        "@": resolve(__dirname, 'src'), // 路径别名
    },
    extensions: ['.js', '.json', '.ts'] // 使用路径别名时想要省略的后缀名，可以自己 增减
  },
  // 配置代理，解决跨越问题
  devServer: {
    proxy: {
      "/api":{
        target: "http://",
        rewrite:path => path.replace(/^\/api/,'')
      }
    }
  }
})
